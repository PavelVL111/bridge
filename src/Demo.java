import Abstraction.AdvencedRemote;
import Abstraction.BasicRemote;
import Implementation.Device;
import Implementation.Radio;
import Implementation.TV;

public class Demo {
    public static void main(String[] args) {
//        useDevice(new TV());
        useDevice(new Radio());
    }

    static public void useDevice(Device device){
        BasicRemote basicRemote = new BasicRemote(device);
        basicRemote.togglePower();
        basicRemote.volumeUp();
        basicRemote.volumeUp();
        basicRemote.channelUp();
        basicRemote.channelUp();
        device.printStatus();

        System.out.println();

        AdvencedRemote advencedRemote = new AdvencedRemote(device);
        basicRemote.volumeDown();
        basicRemote.channelDown();
        advencedRemote.mute();
        device.printStatus();

        System.out.println();
        System.out.println();
    }
}
