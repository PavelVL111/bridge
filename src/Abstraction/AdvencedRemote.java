package Abstraction;

import Implementation.Device;

public class AdvencedRemote extends BasicRemote{

    public AdvencedRemote(Device device) {
        super(device);
    }

    public void mute(){
        this.device.setVolume(0);
    }
}
