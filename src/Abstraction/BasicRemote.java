package Abstraction;

import Implementation.Device;

public class BasicRemote implements Remote {

    Device device;

    public BasicRemote(Device device) {
        this.device = device;
    }

    @Override
    public void togglePower() {
        if (this.device.isEnebled()) {
            this.device.disable();
        } else {
            this.device.enable();
        }
    }

    @Override
    public void volumeUp() {
        this.device.setVolume(this.device.getVolume() + 10);
    }

    @Override
    public void volumeDown() {
        this.device.setVolume(this.device.getVolume() - 10);
    }

    @Override
    public void channelUp() {
        this.device.setChanel(device.getChanel() + 1);
    }

    @Override
    public void channelDown() {
        this.device.setChanel(device.getChanel() - 1);
    }
}
