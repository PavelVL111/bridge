package Implementation;

public class Radio implements Device {

    public boolean togglePower;

    public int volume;

    public int channel;

    @Override
    public boolean isEnebled() {
        return this.togglePower;
    }

    @Override
    public void enable() {
        this.togglePower = true;
    }

    @Override
    public void disable() {
        this.togglePower = false;
    }

    @Override
    public int getVolume() {
        return this.volume;
    }

    @Override
    public void setVolume(int volume) {
        this.volume = volume;
    }

    @Override
    public int getChanel() {
        return this.channel;
    }

    @Override
    public void setChanel(int channel) {
        this.channel = channel;
    }

    @Override
    public void printStatus() {
        System.out.println("Radio Toggle: " + togglePower);
        System.out.println("Radio Volume: " + volume);
        System.out.println("Radio Channel: " + channel);
    }
}
