package Implementation;

public interface Device {
    boolean isEnebled();

    void enable();

    void disable();

    int getVolume();

    void setVolume(int channel);

    int getChanel();

    void setChanel(int channel);

    void printStatus();
}
